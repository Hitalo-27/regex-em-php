<?php

$arquivo = "teste.txt";

// Ler o conteudo do arquivo teste.txt
$fp = fopen($arquivo, "r");
$texto = "";
if (filesize($arquivo) > 0)
    $texto = fread($fp, filesize($arquivo));
fclose($fp);


// Filtro do arquivo teste.txt
$source = $texto;

// Filtro dos produtos
$patternProdutos = '/\w{4,}.* gb \w{1,}.*/i';
$result = preg_match_all($patternProdutos, $source, $produtos);

// Filtro dos valores
$patternValor = '/^R\p{Sc}\d{1}\.\d{3}|^R\p{Sc}[3-9]{3}/m';
$result2 = preg_match_all($patternValor, $source, $valor);

// Filtro do Frete
$patternFrete = '/^Frete.*/m';
$result3 = preg_match_all($patternFrete, $source, $frete);

// Filtro da Quantidade de Parcelas
$patternQtdeParcelas = '/^[0-9]+x/m';
$result4 = preg_match_all($patternQtdeParcelas, $source, $qtdeParcelas);

$keyArr = ['produto', 'valor', 'frete', 'qtdeParcelas'];
for ($i = 0; $i < count($produtos[0]); $i++) {
    $array[$i] = array('Produtos' => $produtos[0][$i], 'Valor' => $valor[0][$i], 'Frete' => $frete[0][$i], 'QtdeParcelas' => $qtdeParcelas[0][$i]);
    $arrayFinal[$i] = preg_replace('/\n|\t|\r/', "", $array[$i]);

}

// Apresentação em html
//echo "<pre>"; print_r($arrayFinal);echo"</pre>";

// Apresentação em Json
echo json_encode($arrayFinal, JSON_UNESCAPED_UNICODE);






